import json
import os

import requests
from django.shortcuts import render
from django.views.generic import TemplateView, View
from django.views.generic.edit import FormView

from django_frontend.settings import KEEPER_SERVICE, REAPER_SERVICE

from .forms import SetScrapperForm


HEADERS_CONTENT = {
            'Content-Type': 'application/json',
            'User-Agent': 'PostmanRuntime/7.24.1',
            'Accept': '*/*',
            'Connection': 'keep-alive'}

REAPER_RUN_PARCER = os.path.join(REAPER_SERVICE, 'api/parser/run')
KEEPER_GET_VACANCIES = os.path.join(KEEPER_SERVICE, 'api/vacancies', '')


class HomePageView(TemplateView):
    template_name = "home.html"


class SetScrapperView(FormView):
    template_name = "scrapper_search.html"
    form_class = SetScrapperForm
    success_url = '/scrapper_results/'

    
class ResultScrapperView(View):
    def get(self, request):
        params = {'request_keyword': request.GET['request_keyword'],
                  'searched_words': request.GET['searched_words']}
        response = requests.get(REAPER_RUN_PARCER,
                                headers=HEADERS_CONTENT, params=params)
        context = {}
        if response.status_code == 200:
            context.update(json.loads(response.content))
        else:
            context.update({'message': 'Error on the server side. Please try '
                                       'again.'})
        return render(request, "scrapper_results.html", context)


class ReportsView(View):
    def get(self, request):
        response = requests.get(KEEPER_GET_VACANCIES, headers=HEADERS_CONTENT)
        json_data = json.loads(response.content)
        context = {'result_data_list': json_data['results']}
        return render(request, "scrapper_reports.html", context)
