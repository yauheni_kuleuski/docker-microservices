from django.urls import path
from django.views.generic import TemplateView
from web_interface.views import \
     SetScrapperView, ResultScrapperView, \
          HomePageView, ReportsView


urlpatterns = [
     path('', HomePageView.as_view(), name='home'),
     path('search/', SetScrapperView.as_view(), name='search'),
     path('results/', ResultScrapperView.as_view(), name='results'),
     path('reports/', ReportsView.as_view(), name='reports'),
]
