from django import forms


class SetScrapperForm(forms.Form):
    request_keyword = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': 'python',
                'size': '25'}))
    searched_words = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': 'python, linux, flask',
                'size': '50'}))