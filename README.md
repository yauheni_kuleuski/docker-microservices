# Auriga

Microservices app usinng Docker / docker-compose
*  Reaper (service 1): scrapes data from web sites, parses data, stores date using Keeper
*  Keeper (service 2): manages db that stores date provided by Reaper, provides interface for Master in order to retrieve data
*  Master (service 3): gets data from Keeper, visualises it, sends requests to Reaper to run web scrapper

Code should be written according to OOP principles.

*Start project*
```docker
$ docker-compose run keeper-django-rest python manage.py migrate
$ docker-compose up -d --build
```

Check: http://localhost:8080/