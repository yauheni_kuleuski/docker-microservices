Keeper (service 2): 
===================

Manages db that stores date provided by Reaper, provides interface for Master in order to retrieve data.

Solution:
* Use a Django REST Framework to access PostgreSQL database