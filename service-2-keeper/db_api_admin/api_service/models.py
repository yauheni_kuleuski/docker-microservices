from django.db import models
from django.contrib.postgres.fields import JSONField, ArrayField


class VacancyModel(models.Model):
    website = models.URLField()
    vacancy_url = models.URLField()
    request_keyword = models.CharField(max_length=25)
    searched_words = ArrayField(models.CharField(max_length=200), blank=True)
    result_parsing = JSONField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:  
        app_label = "api_service"

    def __str__(self):
        return '[ %s ]' % self.vacancy_url
