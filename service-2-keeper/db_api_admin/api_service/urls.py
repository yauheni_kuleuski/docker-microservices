from rest_framework import routers
from django.urls import include, path
from .views import \
    UserViewSet, VacancyViewSet

router = routers.DefaultRouter()
router.register('users', UserViewSet)
router.register('vacancies', VacancyViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls',
                              namespace='rest_framework'))
]