from django.contrib.auth.models import User
from .models import VacancyModel
from rest_framework import viewsets
from .serializers import \
    UserSerializer, VacancySerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class VacancyViewSet(viewsets.ModelViewSet):
    queryset = VacancyModel.objects.all()
    serializer_class = VacancySerializer
