from rest_framework import serializers
from django.contrib.auth.models import User
from api_service.models import VacancyModel

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'email']


class VacancySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = VacancyModel
        fields = ['id', 'website', 'vacancy_url', 'request_keyword',
                  'searched_words', 'result_parsing', 'created_at', 'updated_at']