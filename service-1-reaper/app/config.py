import os


KEEPER_SERVICE = os.environ.get('KEEPER_SERVICE', default='http://localhost:8000')
KEEPER_GET_VACANCIES = os.path.join(KEEPER_SERVICE, 'api/vacancies', '')
DEBUG = eval(os.environ.get('DEBUG', default='False'))
HOST_NAME = '0.0.0.0'

HEADERS = {
            'Content-Type': 'application/json',
            'User-Agent': 'PostmanRuntime/7.24.1',
            'Accept': '*/*',
            'Connection': 'keep-alive'}