from bs4 import BeautifulSoup as BS
import requests
import re
import time
from .websites import JobsTutBySite


class Parser:

    def __init__(self, request_keyword, searched_words):
        self.enqueuer_urls = []
        self.result_data_list = []
        self.website = JobsTutBySite()
        self.request_keyword = request_keyword
        self.searched_words = searched_words
        self.result_words_count = {}
    

    def appropriate_search_pattern(self):
        '''If we use "WebSite" class, we don't have search_pattern.'''
        self.website.search_pattern = str(input('Please input' \
            ' the job search page $ '))
        

    def find_vacancies(self):
        '''
        Using the entered keyword, we find the url of the vacancy address.
        '''
        page = 0
        while True:
            time.sleep(1)
            urls_snapshot = len(self.enqueuer_urls)
            pattern = self.website.search_pattern.format(
                key=self.request_keyword, 
                page=page)
            response = requests.get(pattern, headers=self.website.headers)
            soup = BS(response.text, "lxml")

            vacancy_page_list = soup.find('div', {'class': 'vacancy-serp'})
            if vacancy_page_list.contents:
                for vacancy in vacancy_page_list:
                    try:
                        url = vacancy.find('a', {'class': 'bloko-link ' \
                            'HH-LinkModifier'}).get('href')
                        self.enqueuer_urls.append(url)
                    except Exception:
                        url = ''
                if len(self.enqueuer_urls) == urls_snapshot:
                    break
                page += 1
            else:
                break


    def find_count_words(self, url):
        '''
        For each url we find the search words in the text of the vacancy.
        '''
        time.sleep(1)
        response = requests.get(url, headers=self.website.headers)
        result_data_dict = {'url': url,
                            'request_keyword': self.request_keyword}
        soup = BS(response.text, "lxml")
        vacancy_text_block = soup.find('div', {'class': "bloko-columns-row"}) \
            .find_all('div', {'class': "bloko-columns-row"})[:4]
        for div in vacancy_text_block:
            lines_of_text = div.find_all(text=True)
            for line in lines_of_text:
                for word in self.searched_words:
                    result = len(re.findall(word, line, flags=re.IGNORECASE))
                    result_data_dict[word] = \
                        result_data_dict.get(word, 0) + result
        return result_data_dict


    def average_word_occurrence(self):
        '''Calculate the average word occurrence'''
        res_dict = {word: 0 for word in self.searched_words}
        for dataset in self.result_data_list:
            for word in res_dict:
                res_dict[word] += dataset[word]
        for word in res_dict:
            res_dict[word] /= len(self.result_data_list)
            res_dict[word] = round(res_dict[word], 2)
        self.result_words_count.update(res_dict)
        return res_dict


    def __str__(self):
        return '\nSite - %s' % (self.website)


if __name__ == '__main__':
    pass
