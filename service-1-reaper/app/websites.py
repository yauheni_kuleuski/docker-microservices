from bs4 import BeautifulSoup as BS
import requests


class WebSite:
    '''
    Constructor for the standard site for the "Parcer" class. 
    Further we will work only with this object, in which there will be 
    the main data of the site that we will parse.
    '''
    def __init__(self, domain_name):
        self.domain_name = domain_name
        self.headers =  {
            "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537" \
                ".36 (KHTML, like Gecko) Chrome/81.0.4044.129 Safari/537.36",
            "accept-language": "en-US,en;q=0.9,ru;q=0.8"
        }
        try:
            self.responce = requests.get(
                self.domain_name, 
                headers=self.headers)
            self.site_data = BS(self.responce.text, 'lxml')
            self.title = self.site_data.title.string
        except Exception:
            print('Site ', self.domain_name, ' not found.')
            self.title = "Not found"

    def __str__(self):
        return '[ %s - "%s" ]\n' % (self.domain_name, self.title)

class JobsTutBySite(WebSite):
    '''
    Augmented WebSite class. With it, we have more input at the stage 
    of creating the "website" object for the "Parcer" class.
    '''
    def __init__(self):
        WebSite.__init__(self, 'https://jobs.tut.by/')
        self.search_pattern = self.domain_name + \
            'search/vacancy?L_is_autosearch=false&area=1002&clusters=true' \
            '&enable_snippets=true&text={key}&page={page}'


if __name__ == '__main__':
    tut_by = WebSite('https://jobs.tut.by/')
    print(tut_by)
    ps1 = JobsTutBySite()
    print(ps1)
