from flask import Flask, jsonify
from flask import make_response
from flask import request
import os
import json
import requests

from app.config import *
from app.scrapper import Parser

app = Flask(__name__)


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'API request not found'}), 404)


@app.route('/api/parser/run', methods=['GET'])
def run_parser():
    if request.method == 'GET':
        line_separator = lambda x: x.replace(',', '').split()
        searched_words = line_separator(request.args['searched_words'])
        session = Parser(
            request.args['request_keyword'],
            tuple(searched_words))
        session.find_vacancies()
        response_data = []
        for url in session.enqueuer_urls:
            result = session.find_count_words(url)
            session.result_data_list.append(result)
            payload = {
                "website": session.website.domain_name,
                "vacancy_url": url,
                "request_keyword": request.args['request_keyword'],
                "searched_words": searched_words,
                "result_parsing": result
            }
            response_data.append(payload)
            db_response = requests.post(KEEPER_GET_VACANCIES, data=json.dumps(payload),
                                        headers=HEADERS)
        result_words_count = session.average_word_occurrence()

        context = {'result_data_list': response_data,
                   'result_words_count': result_words_count}
        return jsonify(context)


if __name__ == '__main__':
    app.run(debug=DEBUG, host=HOST_NAME)
